# Capture The Flag Key Manager

This is a key manager for the Capture the flag games!

You may ask, what are Capture the flag games?

They are series of challenges with the goal being finding a series of characters named 'flags', each flag unlocks the next level with even more complicated challenges.

They can be about anything, cryptography, quizzes, etc...

## Why use it?

I see people coming in OverTheWire, complaining they've lost their progress because they didn't think they had to save it.

Meanwhile, people who know that they should save it, find it tedious to manually do so, so most write a shell script to do it for them.

The problem with that, is that most are too simplistic to be much useful, others have bad, invasive defaults (Which usually doesn't port to other platforms other than the coder's one)

And a secure Rust program, a handy executable doing it all for you, written with a simplistic GUI/CLI interface, with a sane directory hierarchy as a bonus, would solve all these above to varying degrees.

## How to use

Installing Rust and compiling the program:

1. Install the `rustup` tool from your package manager, or if on Windows, download and execute [this](https://static.rust-lang.org/rustup/dist/i686-pc-windows-gnu/rustup-init.exe) file. It will install Rust for you and you can skip the second step, if unsure type 1 to every prompt.
2. Install the latest stable toolchain of Rust: `rustup default stable`
3. Execute this command: `cargo build --release` (Will take a while, depending on hardware and internet connection)
4. You will find the executable in `target/release/capture-the-flag-key-manager`
5. Run it with the `cargo run --release` command! (If you want the binary, it is under the `target/release` directory)
6. Enjoy!

### Optional feature

If you want the GUI for the program, just run/build it with the `--features gui` arguments! (Not compiled by default for fast compile times and to avoid bloat)

NOTE: As of the latest update, this program works well under Windows! If you were worried about Windows compatibility, you are covered here! Report issues [here](https://gitlab.com/Seven0492/capture-the-flag-key-manager/-/issues)
