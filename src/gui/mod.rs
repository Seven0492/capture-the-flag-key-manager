#![cfg_attr(not(debug_assertions), windows_subsystem = "windows")] // Hide console when on Windows in release
#![cfg(feature = "gui")]

use crate::shared;
use eframe::egui::{self, Id};
use once_cell::sync::OnceCell;

static CONFIG: OnceCell<shared::config::MyConfig> = OnceCell::new();

pub fn run() {
    shared::enable_config(&CONFIG);

    KeyManager::new();
}

#[derive(Default)]
struct KeyManager {
    allowed_to_close: bool,
    show_confirmation_dialog: bool,
    input: String,
    mode: u8,
    flag: String,
    challenge: String,
    warning_prompt: bool,
    error: String,
}

impl KeyManager {
    /// Creates a new KeyManager App instance
    fn new() {
        let options = eframe::NativeOptions {
            // Hide the OS-specific "chrome" around the window:
            decorated: false,
            // To have rounded corners we need transparency:
            transparent: true,
            min_window_size: Some(egui::vec2(320.0, 100.0)),
            resizable: true,
            ..Default::default()
        };

        eframe::run_native(
            "Capture-the-flag key manager",
            options,
            Box::new(|_cc| Box::new(KeyManager::default())),
        );
    }
}

impl eframe::App for KeyManager {
    fn clear_color(&self, _visuals: &egui::Visuals) -> egui::Rgba {
        egui::Rgba::TRANSPARENT // Make sure we don't paint anything behind the rounded corners
    }

    fn on_close_event(&mut self) -> bool {
        self.show_confirmation_dialog = true;
        self.allowed_to_close
    }

    fn update(&mut self, ctx: &egui::Context, frame: &mut eframe::Frame) {
        custom_window_frame(ctx, frame, "Capture-the-flag key manager", |ui| {
            ui.label("Central panel");
            ui.horizontal(|ui| {
                ui.label("Dark/Light theme:");
                egui::widgets::global_dark_light_mode_buttons(ui);
            });
        });

        egui::CentralPanel::default().show(ctx, |ui| {
            // ui.push_id(Id::new("title"), |ui| {
            // ui.heading("Capture-the-flag key manager");
            // ui.add_space(7.0);
            // });

            if self.mode == 0 {
                ui.label("Challenge:");
                ui.push_id(Id::new(10_u8), |ui| {
                    ui.text_edit_singleline(&mut self.challenge);
                });
                ui.add_space(3.0);

                ui.label("Action:");
                ui.add_space(2.0);

                ui.push_id(Id::new(20_u8), |ui| {
                    if ui.button("Access challenge notes").clicked() && !self.challenge.is_empty() {
                        self.input = shared::read_file(
                            &shared::get_config(&CONFIG)
                                .dir_root
                                .join(&self.challenge)
                                .join("notes"),
                            None,
                        )
                        .unwrap_or_default();

                        self.mode = 1;
                    }
                });
                ui.push_id(Id::new(30_u8), |ui| {
                    if ui.button("Access flag").clicked() && !self.challenge.is_empty() {
                        self.input = String::new();

                        self.mode = 2;
                    }
                });
            } else if self.mode == 1 {
                ui.push_id(Id::new(1_u8), |ui| {
                    ui.push_id(11_u8, |ui| {
                        ui.text_edit_multiline(&mut self.input);
                    });

                    ui.add_space(2.0);
                    ui.push_id(Id::new(12_u8), |ui| {
                        if ui.button("Save to notes file").clicked() {
                            if let Err(err) = shared::write_file(
                                &shared::get_config(&CONFIG)
                                    .dir_root
                                    .join(&self.challenge)
                                    .join("notes"),
                                &self.input,
                            ) {
                                self.error = err.to_string();
                                self.warning_prompt = true;
                            }
                        }
                    });
                });
            } else if self.mode == 2 {
                ui.push_id(Id::new(2_u8), |ui| {
                    ui.label("Flag (Positive number):");
                    ui.push_id(Id::new(21_u8), |ui| {
                        ui.text_edit_singleline(&mut self.flag);
                    });

                    if self.flag.parse::<u32>().is_ok() && !self.flag.is_empty() {
                        ui.add_space(5.0);

                        ui.push_id(Id::new(23_u8), |ui| {
                            if ui.button("Load flag").clicked() {
                                self.input = shared::read_file(
                                    &shared::get_config(&CONFIG)
                                        .dir_root
                                        .join(&self.challenge)
                                        .join(&self.flag)
                                        .join("flag"),
                                    Some(&self.flag),
                                )
                                .map_err(|err| {
                                    self.error = format!("Error: {}", &err);
                                    self.warning_prompt = true;
                                })
                                .unwrap_or_default();

                                if self.input.is_empty() && self.error.is_empty() {
                                    self.error = String::from("Flag file present but empty");
                                    self.warning_prompt = true;
                                }
                            }
                        });

                        ui.add_space(2.0);
                        ui.label("Contents:");
                        ui.push_id(Id::new(22_u8), |ui| {
                            ui.text_edit_multiline(&mut self.input);
                        });

                        ui.add_space(2.0);
                        ui.push_id(Id::new(24_u8), |ui| {
                            if ui.button("Set").clicked() {
                                if let Err(err) = shared::write_file(
                                    &shared::get_config(&CONFIG)
                                        .dir_root
                                        .join(&self.challenge)
                                        .join(&self.flag)
                                        .join("flag"),
                                    &self.input,
                                ) {
                                    self.error = format!("Error: {}", &err);
                                    self.warning_prompt = true;
                                }
                            }
                        });
                    } else if !self.flag.is_empty() {
                        ui.label("Invalid flag");
                    }
                });
            }

            if self.mode != 0 {
                ui.push_id(Id::new(format!("footer_{}", &self.mode)), |ui| {
                    ui.add_space(5.0);
                    ui.push_id(Id::new("footer_button"), |ui| {
                        if ui.button("Return to menu").clicked() {
                            self.input = String::new();
                            self.flag = String::new();
                            self.mode = 0;
                        }
                    });
                });
            }
        });

        if self.warning_prompt {
            egui::Window::new("Warning")
                .collapsible(false)
                .resizable(false)
                .id(Id::new("Warning_prompt"))
                .show(ctx, |ui| {
                    ui.horizontal(|ui| {
                        ui.label(self.error.to_string());
                    });
                    ui.horizontal(|ui| {
                        if ui.button("Close").clicked() {
                            self.warning_prompt = false;
                        }
                    });
                });
        }

        if self.show_confirmation_dialog {
            // Show confirmation dialog:
            egui::Window::new("Do you want to quit?")
                .collapsible(false)
                .resizable(false)
                .id(Id::new("Quit_confirmation_windows"))
                .show(ctx, |ui| {
                    ui.horizontal(|ui| {
                        if ui.button("Cancel").clicked() {
                            self.show_confirmation_dialog = false;
                        }

                        if ui.button("Yes!").clicked() {
                            self.allowed_to_close = true;
                            frame.close();
                        }
                    });
                });
        }
    }
}

fn custom_window_frame(
    ctx: &egui::Context,
    frame: &mut eframe::Frame,
    title: &str,
    add_contents: impl FnOnce(&mut egui::Ui),
) {
    use egui::*;
    let text_color = ctx.style().visuals.text_color();

    // Height of the title bar
    let height = 28.0;

    CentralPanel::default()
        .frame(Frame::none())
        .show(ctx, |ui| {
            let rect = ui.max_rect();
            let painter = ui.painter();

            // Paint the frame:
            painter.rect(
                rect.shrink(1.0),
                10.0,
                ctx.style().visuals.window_fill(),
                Stroke::new(1.0, text_color),
            );

            // Paint the title:
            painter.text(
                rect.center_top() + vec2(0.0, height / 2.0),
                Align2::CENTER_CENTER,
                title,
                FontId::proportional(height * 0.8),
                text_color,
            );

            // Paint the line under the title:
            painter.line_segment(
                [
                    rect.left_top() + vec2(2.0, height),
                    rect.right_top() + vec2(-2.0, height),
                ],
                Stroke::new(1.0, text_color),
            );

            // Add the close button:
            let close_response = ui.put(
                Rect::from_min_size(rect.left_top(), Vec2::splat(height)),
                Button::new(RichText::new("❌").size(height - 4.0)).frame(false),
            );
            if close_response.clicked() {
                frame.close();
            }

            // Interact with the title bar (drag to move window):
            let title_bar_rect = {
                let mut rect = rect;
                rect.max.y = rect.min.y + height;
                rect
            };
            let title_bar_response =
                ui.interact(title_bar_rect, Id::new("title_bar"), Sense::click());
            if title_bar_response.is_pointer_button_down_on() {
                frame.drag_window();
            }

            // Add the contents:
            let content_rect = {
                let mut rect = rect;
                rect.min.y = title_bar_rect.max.y;
                rect
            }
            .shrink(4.0);
            let mut content_ui = ui.child_ui(content_rect, *ui.layout());
            add_contents(&mut content_ui);
        });
}
