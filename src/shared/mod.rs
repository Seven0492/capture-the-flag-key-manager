pub mod config {
    use dirs::home_dir;
    use serde::{Deserialize, Serialize};
    use std::fmt::Formatter;
    use std::path::PathBuf;
    use std::process::exit;

    #[derive(Debug, Clone, Deserialize, Serialize)]
    pub struct MyConfig {
        pub dir_root: PathBuf,
        pub bypass_os_warning: Option<bool>,
    }

    /// `MyConfig` implements `Display`
    impl std::fmt::Display for MyConfig {
        fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
            let MyConfig {
                dir_root,
                bypass_os_warning,
            } = self;

            write!(
                f,
                "Operations directory: \"{}\"\
        \nBypass OS warning: {}",
                dir_root.display(),
                bypass_os_warning.unwrap_or(false)
            )
        }
    }

    /// `MyConfig` implements `Default`
    impl ::std::default::Default for MyConfig {
        fn default() -> Self {
            Self {
                dir_root: {
                    if cfg!(unix) {
                        PathBuf::from("/tmp/flag_manager")
                    } else if cfg!(Windows) {
                        PathBuf::from(r"%userprofile%\AppData\Local\Temp")
                    } else {
                        PathBuf::from({
                            format!(
                                "{}",
                                home_dir()
                                    .unwrap_or_else(|| {
                                        eprintln!("Failed to determine home directory\n");

                                        exit(exitcode::OSERR);
                                    })
                                    .join("Documents")
                                    .join("flag_manager")
                                    .display()
                            )
                        })
                    }
                },
                bypass_os_warning: Some(false),
            }
        }
    }
}

use anyhow::Result;
use confy::{self, ConfyError};
use once_cell::sync::OnceCell;
use os_error::OsError;
use std::fs::{create_dir_all, write, File};
use std::io::Read;
use std::path::PathBuf;
use std::process::exit;

/// Makes sure the directory specified the `dir` input variable exists,
/// if not, creates it and all parent directories, with error handling.
pub fn make_dir(dir: &PathBuf, silent: bool) {
    match create_dir_all(dir) {
        Err(err) => {
            if OsError::last_os_error().kind().to_string() == "AlreadyExists" {
                if dir.is_file() {
                    eprintln!("\nFailed to create directory, file at position of root directory");

                    exit(exitcode::IOERR);
                } else if !dir.is_dir() {
                    eprintln!(
                        "\nFailed to create directory, \
                    unknown object at root directory blocking creation:\
                    \n {:?}",
                        &err
                    );

                    exit(exitcode::UNAVAILABLE);
                } else if !silent {
                    println!("Operations directory secured");
                }
            } else {
                eprintln!("Unknown/non-handled error: {:?}", &err);

                exit(exitcode::UNAVAILABLE);
            }
        }
        Ok(_) => {
            if !silent {
                println!("Operations directory secured");
            }
        }
    }
}

/// Enable config
pub fn enable_config(config: &OnceCell<config::MyConfig>) {
    if let Err(err) = config.set({
        match confy::load("flag_manager", None) {
            Ok(value) => value,
            Err(ConfyError::BadTomlData(err)) => {
                eprintln!("Bad config file format: {}\n", &err);

                exit(exitcode::CONFIG);
            }
            Err(err) => {
                eprintln!("Loading config file error: {}\n", &err);

                exit(exitcode::UNAVAILABLE);
            }
        }
    }) {
        eprintln!(
            "Error enabling config (seting value to OnceCell): \n{}\n",
            &err
        );

        exit(exitcode::IOERR);
    };
}

/// Returns MyConfig struct contained in OnceCell<config::MyConfig>
pub fn get_config(config: &OnceCell<config::MyConfig>) -> config::MyConfig {
    config
        .get()
        .map_or_else(|| config::MyConfig::default(), |config| config.clone())
}

/// Read and print file contents with error handling
pub fn read_file(file_path: &PathBuf, level: Option<&String>) -> Result<String, std::io::Error> {
    let mut file_contents = String::new();

    make_dir(
        &file_path
            .parent()
            .unwrap_or_else(|| {
                eprintln!(
                    "Failed getting file path's directory: {}",
                    &file_path.display()
                );

                exit(exitcode::DATAERR);
            })
            .to_path_buf(),
        true,
    );

    match File::open(file_path) {
        Err(err) => {
            eprintln!("Error:");
            eprintln!("Failed to open file: {}", &err);
            eprintln!("At location: \"{}\"\n", &file_path.display());

            Err(err)
        }
        Ok(mut file) => match file.read_to_string(&mut file_contents) {
            Err(err) => {
                eprintln!("Error:");
                eprintln!("Failed to read file: {}", &err);
                eprintln!("At location: \"{}\"\n", &file_path.display());

                Err(err)
            }
            Ok(_) => {
                if level.is_some() {
                    println!("Flag {}:", &level.unwrap());
                } else {
                    println!("Contents:");
                }
                println!(
                    "---------\
                    \n{}\
                    \n---------\n",
                    &file_contents
                );

                Ok(file_contents)
            }
        },
    }
}

pub fn write_file(file_path: &PathBuf, contents: &String) -> Result<(), std::io::Error> {
    match write(file_path, contents) {
        Err(err) => {
            eprintln!("File path: {}", file_path.display());
            eprintln!("Failed to write to file: {}\n", &err);

            Err(err)
        }
        Ok(_) => {
            println!("Contents written to file successfuly\n");

            Ok(())
        }
    }
}
