use anyhow::{anyhow, Result};
use clap::Parser;
use once_cell::sync::OnceCell;
use std::env::var;
use std::fs::write;
use std::io::{stdin, stdout, Write};
use std::option::Option;
use std::path::PathBuf;
use std::process::{exit, Command};

pub mod data {
    use std::fmt::Formatter;
    use std::path::PathBuf;

    pub struct Data {
        pub challenge: PathBuf,
        pub notes: PathBuf,
        pub response: String,
        pub level: String,
    }

    impl Data {
        pub fn new(challenge: PathBuf, notes: PathBuf, response: String, level: String) -> Data {
            Data {
                challenge,
                notes,
                response,
                level,
            }
        }
    }

    /// `Data` implements `Display`
    impl std::fmt::Display for Data {
        fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
            let Data {
                challenge,
                notes,
                response,
                level,
            } = self;

            write!(
                f,
                "Challenge: {}\
        \nNotes: {}\
        \nResponse: {}\
        \nLevel: {}",
                challenge.display(),
                notes.display(),
                response,
                level
            )
        }
    }
}

#[derive(Parser, Debug)]
#[command(author, version, about, long_about = None)]
struct Args {
    /// Switches to CLI mode
    #[arg(short, long, value_name = "BOOLEAN")]
    cli: bool,
}

#[cfg(feature = "gui")]
use capture_the_flag_key_manager::gui;
use capture_the_flag_key_manager::shared;

static CONFIG: OnceCell<shared::config::MyConfig> = OnceCell::new();

fn main() -> Result<()> {
    let args = Args::parse();

    shared::enable_config(&CONFIG);

    if args.cli || !cfg!(feature = "gui") {
        if shared::get_config(&CONFIG).dir_root == shared::config::MyConfig::default().dir_root {
            if cfg!(Unix) || !cfg!(Windows) && !cfg!(Unix) {
                println!(
                "The operations directory, at default, is set to the inside of the /tmp root dir,\
                   \nwhich is wiped every shutdown, please change in config file for anything \
                   more than one-time usage!\n"
            );
            } else if cfg!(Windows) {
                println!(
                "The operations directory, at default, is set to the inside of the '%userprofile%\\AppData\\Local\\Temp' dir,\
                   \nwhich is wiped every shutdown, please change in config file for anything \
                   more than one-time usage!\n"
            );
            }
        }

        os_warning()?;

        let config_file_path = confy::get_configuration_file_path("flag_manager", None)?;

        println!("Config file path: \"{}\"", config_file_path.display());

        // println!("{:?}", &*CONFIG); // Prints actual MyConfig struct

        println!();
        // println!("{}", CONFIG.dir_root.display());

        shared::make_dir(&shared::get_config(&CONFIG).dir_root, false);

        let mut challenge = String::new();

        // If environment variable ${challenge} does not exist or its value is invalid
        if var("challenge").is_err() {
            eprintln!(
                "\n\"challenge\" environment variable not initialised \
            or contains invalid value"
            );

            println!("Challenge to access (Refer to brainstorm.md for explanation):");

            capture_input(&mut challenge);

            if challenge.is_empty() {
                eprintln!("No input detected, please try again:");

                capture_input(&mut challenge);

                if challenge.is_empty() {
                    eprintln!("No input detected, exiting");

                    exit(exitcode::NOINPUT);
                }
            }

            if challenge == "q" || challenge == "quit" {
                println!("Program shutdown!");

                exit(exitcode::OK);
            }
        }

        let operation_dir = &shared::get_config(&CONFIG)
            .dir_root
            .join(PathBuf::from(&challenge));

        shared::make_dir(operation_dir, false);

        println!();

        let data = data::Data::new(
            PathBuf::from(&challenge),
            shared::get_config(&CONFIG)
                .dir_root
                .join(&challenge)
                .join("notes"),
            String::new(),
            String::new(),
        );

        menu(data)?;
    } else {
        #[cfg(feature = "gui")]
        gui::run();
    }

    Ok(())
}

/// Offers the options menu and orchestrates according to the response
fn menu(mut data: data::Data) -> Result<()> {
    println!("[E]dit challenge notes, [p]rint notes, [r]ead flag, [s]et flag, [q]uit:");

    capture_input(&mut data.response);

    if data.response.is_empty() {
        eprintln!("No input detected, please try again:");

        capture_input(&mut data.response);

        if data.response.is_empty() {
            eprintln!("No input detected, exiting.");

            exit(exitcode::NOINPUT);
        }
    }

    if data.response.to_lowercase() == "q" || data.response.to_lowercase() == "quit" {
        println!("Program shutdown!");

        exit(exitcode::OK);
    }

    // println!("response: {}", &data.response);
    // println!("response: {:?}", data.response.to_lowercase());

    if (data.response.to_lowercase() == "r" || data.response.to_lowercase() == "read")
        || (data.response.to_lowercase() == "s" || data.response.to_lowercase() == "set")
    {
        println!("Level to handle:");

        capture_input(&mut data.level);

        if data.level.is_empty() {
            println!("Please input a level!");

            capture_input(&mut data.level);

            if data.level.is_empty() {
                println!("No input detected, exiting.");

                exit(exitcode::NOINPUT);
            }
        } else if data.level == "q" || data.level == "quit" {
            println!("Program shutdown!");

            exit(exitcode::OK);
        }
    }

    let flag_path = shared::get_config(&CONFIG)
        .dir_root
        .join(&data.challenge)
        .join(PathBuf::from(&data.level))
        .join("flag");

    if data.response.to_lowercase() == "e" || data.response.to_lowercase() == "edit" {
        let editor = var("EDITOR");

        let editor = editor.ok().unwrap_or_else(|| {
            String::from({
                if cfg!(unix) {
                    println!("nano terminal text editor");
                    "nano"
                } else if cfg!(windows) {
                    println!("notepad text editor");
                    "notepad"
                } else {
                    println!("vi terminal text editor");
                    "vi"
                }
            })
        });

        let mut child = Command::new(&editor)
            .args([&data
                .notes
                .to_str()
                .unwrap_or_else(|| {
                    eprintln!(
                        "Error converting notes PathBuf to String: {}",
                        &data.notes.display()
                    );

                    exit(exitcode::UNAVAILABLE);
                })
                .to_string()])
            .spawn()
            .expect("Unexpected editor error");

        child
            .wait()
            .expect("Unknown error happened to child program");

        print_info(&data.challenge)?;

        println!();

        menu(data)?;
    } else if data.response.to_lowercase() == "p" || data.response.to_lowercase() == "print" {
        if let Err(err) = shared::read_file(&data.notes, None) {
            eprintln!("Failed to read challenge notes: {}", &err);
        };

        menu(data)?;
    } else if data.response.to_lowercase() == "r" || data.response.to_lowercase() == "read" {
        if let Err(err) = shared::read_file(&flag_path, Some(&data.level)) {
            eprintln!("Failed to read flag file: {}", &err);
        };

        menu(data)?;
    } else if data.response.to_lowercase() == "s" || data.response.to_lowercase() == "set" {
        shared::make_dir(
            &PathBuf::from({
                if flag_path.parent().is_some() {
                    flag_path.parent().unwrap()
                } else {
                    eprintln!(
                        "Failed to get the directory of the flag file {}",
                        &data.level
                    );

                    exit(exitcode::DATAERR);
                }
            }),
            false,
        );

        println!("\nFlag to set:");

        let mut flag = String::new();
        capture_input(&mut flag);

        if flag.is_empty() {
            println!("Please input a level!");

            capture_input(&mut flag);

            if flag.is_empty() {
                println!("No input detected, exiting.");

                exit(exitcode::NOINPUT);
            }
        } else if flag == "q" || flag == "quit" {
            println!("Program shutdown!");

            exit(exitcode::OK);
        }

        match write(
            shared::get_config(&CONFIG).dir_root.join(&flag_path),
            &mut flag,
        ) {
            Err(err) => {
                eprintln!(
                    "Flag path: {}",
                    &CONFIG
                        .get()
                        .ok_or_else(|| anyhow!("Empty configuration"))?
                        .dir_root
                        .join(&flag_path)
                        .display()
                );
                eprintln!("Failed to write to file: {}\n", &err);

                menu(data)?;
            }
            Ok(_) => {
                menu(data)?;
            }
        }
    } else {
        eprintln!("Invalid option!\n");

        menu(data)?;
    }

    Ok(())
}

/// Warns about OS concerns, for every OS except unix (Which I, the maintainer can test and debug).
/// Can be skipped via the `bypass_os_warning` field in the config file
fn os_warning() -> Result<()> {
    let config_path = confy::get_configuration_file_path("flag_manager", None);

    if shared::get_config(&CONFIG).bypass_os_warning == Some(true) {
        println!("Bypassed os warning");
    } else if !cfg!(windows) && !cfg!(unix) {
        eprintln!(
            "I am sorry to say this, but I have not added your OS's support yet in this script, \
        run at your own risk,\nif you want to be a beta tester to have it on your OS,\
        \nI'd be happy to add it if you volunteer!"
        );

        println!(
            "To disable warning and run program, modify `bypass_os_warning` in config:\
        \n {:?}",
            &config_path
        );

        exit(exitcode::TEMPFAIL);
    } else if cfg!(windows) {
        println!(
            "You are running this program under Windows!\
            \nDo not fret! It works fine in Windows, just be aware that hotfixes for \
            Windows compatibility will take longer! (Gitlab issues are encouraged!)"
        );

        println!(
            "To disable warning, modify the `bypass_os_warning` field in the config file:\
            \n {:?}\n",
            &config_path
        );
    }

    Ok(())
}

/// Prints given dir and file path of the info file (presumably) present in the directory
fn print_info(challenge: &PathBuf) -> Result<()> {
    println!(
        "\n\
        USER NOTES LOCATION\
        \n------------\
        \n{}",
        shared::get_config(&CONFIG)
            .dir_root
            .join(challenge)
            .join("notes")
            .display()
    );

    Ok(())
}

/// Uses `read_line()` but with error handling and a placeholder for style.
fn capture_input(response: &mut String) {
    *response = String::new();

    print!(" ==> ");

    stdout().flush().unwrap_or_else(|err| {
        eprintln!(
            "Failed to flush stdout: \"{}\" (Heavily recommended to post an issue on Gitlab)\n",
            &err
        );

        exit(exitcode::OSERR);
    });

    if let Err(err) = stdin().read_line(response) {
        eprintln!("Failed to capture input: {}", &err);

        exit(exitcode::IOERR);
    };

    *response = response.trim().parse().unwrap_or_else(|err| {
        eprintln!(
            "Failed to parse primitive string slice to owned String while writing input to buffer: {}",
            &err
        );

        exit(exitcode::UNAVAILABLE);
    });
    // println!("{}", &response);
    println!();
}

/// Uses `read_line()` but with error handling and without a placeholder,
/// contrary to `capture_input()`.
fn _capture_input_bare(response: &mut String) {
    *response = String::new();

    if let Err(err) = stdin().read_line(response) {
        eprintln!("Failed to capture input: {}", &err);

        exit(exitcode::IOERR);
    }

    *response = response.trim().parse().unwrap_or_else(|err| {
        eprintln!(
            "Failed to parse primitive string to owned String while writing input to buffer: {}",
            &err
        );

        exit(exitcode::UNAVAILABLE);
    });
}
