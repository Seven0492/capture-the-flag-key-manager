## Capture the flag key manager, pseudo-code

### Stage 1: What do I want to do?

Well, first of all, I want it to be able to:

    - Read/Write to files in an organized directory structure. `Directory > Challenge dir > Note and Flags file` (Containing flags and notes for future use)

        - When selecting which level fiels to print, asks for a number, a `[number]-[number]` (Number to number) or a star (*) wildcard to print all in Challenge

    - Be able to repeat operation to not have to execute it again

    - Have a configuration file to store the working directorie's place, specified by the user with a prompt at the initial setup phase

Do I need any dependencies to achieve my purpose?

    - No, I do not beyond the basic UNIX commands

### Stage 2: Rough sketches

1. Create directory hierarchy for levels

```
Chosent root dir > Challenge dir > Note and Flags file

```

2. Do for loop on each file found, up to MAXLEVEL

3. Print flag found for each completed task

```
Challenge: bandit

    bandit0=[flag]
    bandit1=[flag]
    ...

Challenge: ...

```

4. Stop on last task if no flag has been found

5. Print out info file (Info about specific task) for that task and prompt for next command

6. Can choose quit, add flag, add info (Starts terminal editor determined in $EDITOR)

```
| (Tasks possible: Quit, Add flag, Add info/Take note)
| ==> 

```

## Past the sketches

#### Actual directory structure

`Chosen root dir (Default /tmp/flag_manager on Unix) > challenge dir > level dir > flag file`

### Useful to know

- Notes feature does not currently work, work is being done to fix it

- Exit codes abide by the FreeBSD's sysexits.h standard
